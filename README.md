## Installing Postgresql on MacOS
  * Going to [Postgresql's website](www.postgresql.org) and download Postgresql.app

## Create a new Rails Project and configure to app
  * Create rails project with postgresql

    ``` rails new [projectName] -d postgresql ```
  * Open Project and run bundle command

    ``` bundle install ```
  * Scaffolding

    ``` rails g scaffold Person name lastname ```

  * Create Postgresql database

    ``` rails db:create ```

  * Migrate databsae 

    ``` rails db:migrate ```



